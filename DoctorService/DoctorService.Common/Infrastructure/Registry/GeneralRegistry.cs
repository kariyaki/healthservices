﻿using log4net;

namespace DoctorService.Common.Infrastructure.Registry
{
    public class GeneralRegistry : StructureMap.Registry
    {
        public GeneralRegistry()
        {
            For<ILog>().Use(s => LogManager.GetLogger(s.RootType));

        }
    }
}