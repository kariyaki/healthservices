﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Contracts;

namespace DoctorService.Repository.DAL
{
    public class DoctorServiceContext : BaseContext<DoctorServiceContext>
    {

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Consultation> Consultations { get; set; }
        public DbSet<Symptom> Symptoms { get; set; }
        public DbSet<GeneralPractitionerOffice> GeneralPractitionerOffices { get; set; }
        public DbSet<ConsultationPreperation> ConsultationPreperations { get; set; }
        public DbSet<OtherSubject> OtherSubjects { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<BankIdUser> BankIdUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Consultation>()
                .HasOptional(s => s.ConsultationPreperation);
        }
    }
}