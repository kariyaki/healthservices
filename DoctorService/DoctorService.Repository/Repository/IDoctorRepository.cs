﻿using Contracts;

namespace DoctorService.Repository.Repository
{
    public interface IDoctorRepository : IRepository<Doctor, string>
    {
    }
}