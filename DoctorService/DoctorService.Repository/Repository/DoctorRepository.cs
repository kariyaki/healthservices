﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts;
using DoctorService.Repository.DAL;
using log4net;
using Newtonsoft.Json;

namespace DoctorService.Repository.Repository
{
    public class DoctorRepository : IDoctorRepository
    {
        private readonly DoctorServiceContext _dbContext;
        private readonly ILog _logger;

        public DoctorRepository(DoctorServiceContext dbContext, ILog logger)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _dbContext = dbContext;
            _logger = logger;
        }

        public Doctor Get(string doctorssn)
        {
            var doctor = _dbContext.Doctors
                .FirstOrDefault(t => t.Ssn == doctorssn);
            return doctor;
        }

        public Doctor Save(Doctor entity)
        {
            _logger.Debug(JsonConvert.SerializeObject(entity));
            var old = Get(entity.Ssn);
            
            if (old != null)
            {
                _dbContext.Entry(old).CurrentValues.SetValues(entity);
            }
            else
            {
                _dbContext.Entry(entity).State = EntityState.Added;
            }
            _dbContext.SaveChanges();
            return entity;
        }

        public IEnumerable<Doctor> FindAll()
        {
            return _dbContext.Doctors
                 .ToList();
        }

        public void Delete(Doctor entity)
        {
            _dbContext.Doctors.Remove(entity);
            _dbContext.SaveChanges();
        }
    }
}