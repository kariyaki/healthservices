namespace Service.Repository.Migrations.PatientServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SympOthRelaFixx : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.Symptom", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation");
            DropIndex("dbo.OtherSubject", new[] { "ConsultationPreperation_Id1" });
            DropIndex("dbo.Symptom", new[] { "ConsultationPreperation_Id1" });
            RenameColumn(table: "dbo.OtherSubject", name: "ConsultationPreperation_Id1", newName: "ConsultationPreperationId");
            RenameColumn(table: "dbo.Symptom", name: "ConsultationPreperation_Id1", newName: "ConsultationPreperationId");
            AlterColumn("dbo.OtherSubject", "ConsultationPreperationId", c => c.Long(nullable: false));
            AlterColumn("dbo.Symptom", "ConsultationPreperationId", c => c.Long(nullable: false));
            CreateIndex("dbo.OtherSubject", "ConsultationPreperationId");
            CreateIndex("dbo.Symptom", "ConsultationPreperationId");
            AddForeignKey("dbo.OtherSubject", "ConsultationPreperationId", "dbo.ConsultationPreperation", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Symptom", "ConsultationPreperationId", "dbo.ConsultationPreperation", "Id", cascadeDelete: true);
            DropColumn("dbo.OtherSubject", "ConsultationPreperation_Id");
            DropColumn("dbo.Symptom", "ConsultationPreperation_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Symptom", "ConsultationPreperation_Id", c => c.Long(nullable: false));
            AddColumn("dbo.OtherSubject", "ConsultationPreperation_Id", c => c.Long(nullable: false));
            DropForeignKey("dbo.Symptom", "ConsultationPreperationId", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.OtherSubject", "ConsultationPreperationId", "dbo.ConsultationPreperation");
            DropIndex("dbo.Symptom", new[] { "ConsultationPreperationId" });
            DropIndex("dbo.OtherSubject", new[] { "ConsultationPreperationId" });
            AlterColumn("dbo.Symptom", "ConsultationPreperationId", c => c.Long());
            AlterColumn("dbo.OtherSubject", "ConsultationPreperationId", c => c.Long());
            RenameColumn(table: "dbo.Symptom", name: "ConsultationPreperationId", newName: "ConsultationPreperation_Id1");
            RenameColumn(table: "dbo.OtherSubject", name: "ConsultationPreperationId", newName: "ConsultationPreperation_Id1");
            CreateIndex("dbo.Symptom", "ConsultationPreperation_Id1");
            CreateIndex("dbo.OtherSubject", "ConsultationPreperation_Id1");
            AddForeignKey("dbo.Symptom", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation", "Id");
            AddForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation", "Id");
        }
    }
}
