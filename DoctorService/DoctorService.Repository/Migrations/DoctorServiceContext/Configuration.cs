using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using Contracts;

namespace DoctorService.Repository.Migrations.DoctorServiceContext
{
    internal sealed class Configuration : DbMigrationsConfiguration<DAL.DoctorServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            MigrationsDirectory = @"Migrations\DoctorServiceContext";
        }

        protected override void Seed(DAL.DoctorServiceContext context)
        {
            var consultationGuid = Guid.NewGuid();

            var nurse = new Nurse
            {
                Id = 1,
                Ssn = "16117825987",
                FirstName = "Jenny",
                LastName = "Jennysen"
            };

            var doctor = new Doctor
            {
                Id = 1,
                Ssn = "12105022456",
                FirstName = "John",
                LastName = "Johnson",
                Specialization = "Allmenn",
            };

            var patient = new Patient
            {
                Id = 1,
                Ssn = "101112131415",
                FirstName = "Ole",
                LastName = "Normann",
                Address = "Adresse 1",
                Zipcode = "5058",
                City = "Bergen",
                Sex = "Male",
                Birthday = new DateTime(2012, 11, 10),
                Doctor = doctor
            };

            var consultation = new Consultation
            {
                Id = 1,
                Guid = consultationGuid,
                DateTime = new DateTime(2016, 08, 05, 11, 30, 00),
                ConsultationPreperation = new ConsultationPreperation
                {
                    Id = 1,
                    Guid = Guid.NewGuid(),
                    ConsultationTime = new DateTime(2016, 08, 01, 11, 30, 00),
                    NeedForConsultation = "Ja:Fordi jeg �nsker kontroll",
                    ConsultationGuid = consultationGuid,
                    Symptoms = new List<Symptom>
                    {
                        new Symptom
                        {
                            Id = 1,
                            Name = "Lammelse",
                            Change = "Er blitt v�rre i det siste",
                            Description = "Lammelse betyr.... kort men mer utfyllende forklaring",
                            Severity = "Mye ubehag/p�virker hverdagen i stor grad",
                            Important = true
                        }
                    },
                    SymptomListUpdated = "Ja",
                    HasSideEffects = "Ja",
                    NewSideEffectsDegree = "Sterkt plagsomme",
                    OldSideEffectsDegree = "Moderate",
                    SideEffectsNote = "Note note note",
                    SideEffectIsImportant = true,
                    SideEffectsUpdated = "Nei, se bort fra symptomregistreringen",
                    OtherSubjectsList = new List<OtherSubject>
                    {
                        new OtherSubject { Id = 1, Name= "Sp�rsm�l om MS-medisin"},
                        new OtherSubject { Id = 2, Name=  "�nsker informasjon/avgi samtykke til Norsk MS register og biobank"}
                    },
                    OtherSubjectsNote = "Note note note note"
                }
            };

            //var generalPractitionerOffice = new GeneralPractitionerOffice
            //{
            //    Id = 1,
            //    Name = "DoctorOffice AS",
            //    Address = "Addresse 1",
            //    City = "Bergen",
            //    Zipcode = "5045",
            //    Consultations = new List<Consultation> { consultation },
            //    Doctors = new List<Doctor> { doctor },
            //    Nurses = new List<Nurse> { nurse },
            //    Patients = new List<Patient> { patient }
            //};
            //context.GeneralPractitionerOffices.AddOrUpdate(t => t.Id, generalPractitionerOffice);
        }
    }
}
