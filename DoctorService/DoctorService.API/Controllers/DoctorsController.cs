﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using Contracts;
using DoctorService.BusinessLogic;
using log4net;

namespace DoctorService.API.Controllers
{
    /// <summary>
    /// Doctors
    /// </summary>
    [RoutePrefix("api/doctors")]
    public class DoctorsController : ApiController
    {
        private readonly IDoctorControllerService _doctorService;

        private readonly ILog _logger;

        /// <summary>
        /// Doctors constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="doctorService"></param>
        public DoctorsController( ILog logger, IDoctorControllerService doctorService)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (doctorService == null) throw new ArgumentNullException(nameof(doctorService));
            _logger = logger;
            _doctorService = doctorService;
        }

        /// <summary>
        /// Get all doctors
        /// </summary>
        /// <remarks>
        /// Get all doctors
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<Doctor> GetAllDoctors()
        {
            _logger.Debug("GetAllDoctors");
            var all = _doctorService.GetAllDoctors();
            return all;
        }

        /// <summary>
        /// Get doctor
        /// </summary>
        /// <remarks>
        /// Get the doctor object with specified ssn (social security number)
        /// </remarks>
        /// <param name="doctorssn"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Doctor))]
        [Route("{doctorssn}")]
        public IHttpActionResult GetDoctor(string doctorssn)
        {
            _logger.Debug("GetDoctor");

            var doctor = _doctorService.GetDoctor(doctorssn);

            if (doctor != null) return Ok(doctor);
            return NotFound();
        }

        /// <summary>
        /// Create doctor
        /// </summary>
        /// <remarks>
        /// Create a new doctor with specified data
        /// </remarks>
        /// <param name="doctor"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType((typeof(Doctor)))]
        [Route("")]
        public IHttpActionResult CreateDoctor([FromBody]Doctor doctor)
        {
            _logger.Debug("Create doctor");

            var newDoctor = _doctorService.CreateDoctor(doctor);

            if (newDoctor == null) return BadRequest();

            return Created("", newDoctor);
        }

        /// <summary>
        /// Update doctor
        /// </summary>
        /// <remarks>
        /// Update doctor with specified data
        /// </remarks>
        /// <param name="doctor"></param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(Doctor))]
        [Route("")]
        public IHttpActionResult UpdateDoctor([FromBody] Doctor doctor)
        {
            _logger.Debug("Update doctor");
            var updateDoctor = _doctorService.UpdateDoctor(doctor);

            if (updateDoctor != null) return Ok(updateDoctor);
            return BadRequest();
        }

        [HttpDelete]
        [Route("{ssn}")]
        [Obsolete]
        public void DeleteDoctor(string ssn)
        {
            _doctorService.DeleteDoctor(ssn);
        }
    }
}