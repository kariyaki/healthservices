﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using DoctorService.API.Infrastructure;
using DoctorService.API.Utils;
using DoctorService.Common.Infrastructure;

namespace DoctorService.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            RegisterStructureMapActivator(config);
            config.Formatters.Add(new BrowserJsonFormatter());
        }

        private static void RegisterStructureMapActivator(HttpConfiguration config)
        {
            config.Services.Replace(
                typeof(IHttpControllerActivator),
                new StructureMapControllerActivator(StructuremapBootstrapper.Initialize()));
        }
    }
}
