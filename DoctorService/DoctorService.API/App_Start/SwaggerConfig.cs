using System.Web.Http;
using WebActivatorEx;
using DoctorService.API;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace DoctorService.API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration 
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "DoctorService.API");
                        c.IncludeXmlComments(string.Format(@"{0}\bin\DoctorService.API.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IgnoreObsoleteActions();
                    })
                .EnableSwaggerUi(c =>
                    {
                    });
        }
    }
}
