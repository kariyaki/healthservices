﻿using System.Collections.Generic;
using Contracts;

namespace DoctorService.BusinessLogic
{
    public interface IDoctorControllerService
    {

        Doctor GetDoctor(string ssn);
        Doctor CreateDoctor(Doctor doctor);
        Doctor UpdateDoctor(Doctor doctor);

        IEnumerable<Doctor> GetAllDoctors();
        void DeleteDoctor(string ssn);
    }
}