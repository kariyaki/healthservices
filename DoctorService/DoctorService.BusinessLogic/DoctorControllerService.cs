﻿using System;
using System.Collections.Generic;
using Contracts;
using DoctorService.Repository.Repository;
using log4net;

namespace DoctorService.BusinessLogic
{
    public class DoctorControllerService : IDoctorControllerService
    {
        private readonly IDoctorRepository _doctorRepository;
        private readonly ILog _logger;

        public DoctorControllerService(IDoctorRepository doctorRepository, ILog logger)
        {
            if (doctorRepository == null) throw new ArgumentNullException(nameof(doctorRepository));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _doctorRepository = doctorRepository;
            _logger = logger;
        }

        public Doctor GetDoctor(string ssn)
        {
            return _doctorRepository.Get(ssn);
        }

        public Doctor CreateDoctor(Doctor doctor)
        {
            return _doctorRepository.Save(doctor);
        }

        public Doctor UpdateDoctor(Doctor doctor)
        {
            try
            {
                return _doctorRepository.Save(doctor);
            }
            catch (Exception ex)
            {
                _logger.Error("Error updating doctor: " + ex);
                throw;
            }
        }

        public IEnumerable<Doctor> GetAllDoctors()
        {
            return _doctorRepository.FindAll();
        }

        public void DeleteDoctor(string ssn)
        {
            _doctorRepository.Delete(_doctorRepository.Get(ssn));
        }
    }
}