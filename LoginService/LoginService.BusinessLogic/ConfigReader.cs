﻿using System.Configuration;

namespace LoginService.BusinessLogic
{
    public class ConfigReader : IConfigReader
    {
        public string GetValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}