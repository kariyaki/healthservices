﻿using System;
using Contracts;
using LoginService.Repository.Repository;

namespace LoginService.BusinessLogic
{
    public class LoginControllerService : ILoginControllerService
    {
        private readonly ILoginRepository _loginRepository;
        private readonly IAdminRepository _adminRepository;

        public LoginControllerService(ILoginRepository loginRepository, IAdminRepository adminRepository)
        {
            if (loginRepository == null) throw new ArgumentNullException(nameof(loginRepository));
            if (adminRepository == null) throw new ArgumentNullException(nameof(adminRepository));
            _loginRepository = loginRepository;
            _adminRepository = adminRepository;
        }

        public bool ValidUser(BankIdUser user)
        {
            var result = _loginRepository.Get(user.Ssn);
            if (result != null)
            {
                return user.Password == result.Password;
            }
            return false;
        }

        public bool ValidUser(User user)
        {
            var result = _adminRepository.Get(user.Email);
            if (result != null)
            {
                return user.Password == result.Password;
            }
            return false;
        }
    }
}