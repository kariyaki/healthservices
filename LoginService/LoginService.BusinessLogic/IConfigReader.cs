﻿namespace LoginService.BusinessLogic
{
    public interface IConfigReader
    {
        string GetValue(string key);
    }
}