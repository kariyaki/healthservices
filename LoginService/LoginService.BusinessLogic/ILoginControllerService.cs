﻿
using Contracts;

namespace LoginService.BusinessLogic
{
    public interface ILoginControllerService
    {
        bool ValidUser(BankIdUser user);
        bool ValidUser(User user);
    }
}