﻿using System;
using System.Web.Http;
using Contracts;
using LoginService.BusinessLogic;

namespace LoginService.API.Controllers
{
    /// <summary>
    /// Login
    /// </summary>
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        private readonly ILoginControllerService _loginService;

        /// <summary>
        /// Login constructor
        /// </summary>
        /// <param name="loginService"></param>
        public LoginController(ILoginControllerService loginService)
        {
            if (loginService == null) throw new ArgumentNullException(nameof(loginService));
            _loginService = loginService;
        }

        /// <summary>
        /// Login User
        /// </summary>
        /// <remarks>
        /// Authenticates the specified client user.
        /// </remarks>
        /// <param name="user"></param>
        /// <returns>HTTP Ok if user is authenticated, HTTP Unauthorized if not</returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Login([FromBody] BankIdUser user)
        {
            return _loginService.ValidUser(user) ? (IHttpActionResult) Ok() : Unauthorized();
        }
        
        /// <summary>
        /// Login Adminuser
        /// </summary>
        /// <remarks>
        /// Authenticates the specified admin user
        /// </remarks>
        /// <param name="user"></param>
        /// <returns>HTTP Ok if authenticated, HTTP Unauthorized if not</returns>
        [HttpPost]
        [Route("admin")]
        public IHttpActionResult Login([FromBody] User user)
        {
            return _loginService.ValidUser(user) ? (IHttpActionResult)Ok() : Unauthorized();
        }
    }
}