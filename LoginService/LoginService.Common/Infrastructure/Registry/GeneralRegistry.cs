﻿using log4net;

namespace LoginService.Common.Infrastructure.Registry
{
    public class GeneralRegistry : StructureMap.Registry
    {
        public GeneralRegistry()
        {
            For<ILog>().Use(s => LogManager.GetLogger(s.RootType));
        }
    }
}