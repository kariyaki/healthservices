﻿using StructureMap;
using StructureMap.Graph;

namespace LoginService.Common.Infrastructure
{
    public class StructuremapBoostrapper
    {
        public static IContainer Initialize()
        {
            return new Container(x =>
                x.Scan(scan =>
                {
                    scan.LookForRegistries();
                    scan.TheCallingAssembly();
                    scan.AssembliesFromApplicationBaseDirectory(y => y.FullName.StartsWith("LoginService"));
                    scan.WithDefaultConventions();
                }));
        }
    }
}