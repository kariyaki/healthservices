﻿using Contracts;
using LoginService.BusinessLogic;
using LoginService.Repository.Repository;
using Moq;
using NUnit.Framework;

namespace LoginService.Tests
{
    [TestFixture]
    public class LoginServiceTest
    {
        private ILoginControllerService _loginService;
        private Mock<ILoginRepository> _loginRepositoryMock;
        private Mock<IAdminRepository> _adminRepositoryMock;

        [SetUp]
        public void Setup()
        {
            var user = new BankIdUser
            {
                Ssn = "10101010101",
                Password = "Password"
            };
            _loginRepositoryMock = new Mock<ILoginRepository>();
            _adminRepositoryMock = new Mock<IAdminRepository>();
            _loginRepositoryMock.Setup(t => t.Get(user.Ssn)).Returns(user);
            _loginService = new LoginControllerService(_loginRepositoryMock.Object, _adminRepositoryMock.Object);
        }

        [Test]
        public void ShouldReturnTrueOnValidUser()
        {
            var user = new BankIdUser { Ssn = "10101010101", Password = "Password" };
            var result = _loginService.ValidUser(user);
            Assert.IsTrue(result);
        }

        [Test]
        public void ShouldReturnFalseOnInvalidUser()
        {
            var user = new BankIdUser { Ssn = "10101010101", Password = "InvalidPassword" };
            var result = _loginService.ValidUser(user);
            Assert.IsFalse(result);
        }
    }
}
