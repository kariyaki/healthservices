﻿using System.Web.Http.Results;
using Contracts;
using LoginService.API.Controllers;
using LoginService.BusinessLogic;
using Moq;
using NUnit.Framework;

namespace LoginService.Tests
{
    [TestFixture]
    public class LoginControllerTest
    {
        private Mock<ILoginControllerService> _loginServicMock;
        private LoginController _loginController;

        [SetUp]
        public void Setup()
        {
            _loginServicMock = new Mock<ILoginControllerService>();
            _loginController = new LoginController(_loginServicMock.Object);
        }

        [Test]
        public void ShouldReceiveHttpOkOnValidUser()
        {
            var user = new BankIdUser { Ssn = "10101010101", Password = "Password" };
            _loginServicMock.Setup(t => t.ValidUser(user)).Returns(true);

            var loginResult = _loginController.Login(user) as OkResult;
            Assert.IsNotNull(loginResult);
        }

        [Test]
        public void ShouldReceiveHttpUnauthorizedOnInvalidUser()
        {
            var user = new BankIdUser { Ssn = "10101010101", Password = "InvalidPassword" };
            _loginServicMock.Setup(t => t.ValidUser(user)).Returns(false);

            var loginResult = _loginController.Login(user) as UnauthorizedResult;
            Assert.IsNotNull(loginResult);
        }
    }
}
