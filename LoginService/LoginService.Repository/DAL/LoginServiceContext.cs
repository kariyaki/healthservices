﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Contracts;
using Repositories.DAL;

namespace LoginService.Repository.DAL
{
    public class LoginServiceContext : BaseContext<LoginServiceContext>
    {
        public DbSet<BankIdUser> ValidUsers { get; set; }
        public DbSet<User> ValidAdmins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}