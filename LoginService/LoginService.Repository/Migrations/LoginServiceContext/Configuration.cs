using System.Data.Entity.Migrations;
using Contracts;

namespace LoginService.Repository.Migrations.LoginServiceContext
{
    internal sealed class Configuration : DbMigrationsConfiguration<DAL.LoginServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\LoginServiceContext";
        }

        protected override void Seed(DAL.LoginServiceContext context)
        {
            context.ValidAdmins.AddOrUpdate(
                t => t.Email,
                new User() { Email = "admin-hib@email.com", Password = "Pa55word" }
             );
        }
    }
}
