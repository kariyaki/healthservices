namespace Repositories.Migrations.LoginServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankIdUser",
                c => new
                    {
                        Ssn = c.String(nullable: false, maxLength: 128),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Ssn);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BankIdUser");
        }
    }
}
