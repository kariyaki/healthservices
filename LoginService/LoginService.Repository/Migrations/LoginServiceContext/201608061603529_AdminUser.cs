namespace LoginService.Repository.Migrations.LoginServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Email);
        }
        
        public override void Down()
        {
            DropTable("dbo.User");
        }
    }
}
