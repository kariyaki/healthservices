﻿using Contracts;
using PatientService.Repository.Repository;

namespace LoginService.Repository.Repository
{
    public interface IAdminRepository : IRepository<User, string>
    {
    }
}