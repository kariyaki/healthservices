﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts;
using LoginService.Repository.DAL;

namespace LoginService.Repository.Repository
{
    public class LoginRepository : ILoginRepository
    {
        private readonly LoginServiceContext _dbContext;

        public LoginRepository(LoginServiceContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            _dbContext = dbContext;
        }

        public BankIdUser Get(string ssn)
        {
            return _dbContext.ValidUsers.FirstOrDefault(t => t.Ssn == ssn);
        }

        public BankIdUser Save(BankIdUser entity)
        {
            _dbContext.Entry(entity).State = (_dbContext.ValidUsers.Any(e => e.Ssn == entity.Ssn)
                ? EntityState.Modified
                : EntityState.Added);

            _dbContext.SaveChanges();
            return entity;
        }

        public void Delete(BankIdUser entity)
        {
            _dbContext.ValidUsers.Remove(entity);
            _dbContext.SaveChanges();
        }

        public IEnumerable<BankIdUser> FindAll()
        {
            return _dbContext.ValidUsers.ToList();
        }
    }
}