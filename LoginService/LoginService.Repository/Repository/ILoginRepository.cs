﻿using Contracts;
using PatientService.Repository.Repository;

namespace LoginService.Repository.Repository
{
    public interface ILoginRepository : IRepository<BankIdUser, string>
    {
        
    }
}