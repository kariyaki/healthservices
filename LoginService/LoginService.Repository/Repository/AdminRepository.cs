﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts;
using LoginService.Repository.DAL;

namespace LoginService.Repository.Repository
{
    public class AdminRepository : IAdminRepository
    {
        private readonly LoginServiceContext _dbContext;

        public AdminRepository(LoginServiceContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            _dbContext = dbContext;
        }

        public User Get(string email)
        {
            return _dbContext.ValidAdmins.FirstOrDefault(t => t.Email == email);
        }

        public User Save(User entity)
        {
            _dbContext.Entry(entity).State = (_dbContext.ValidAdmins.Any(e => e.Email == entity.Email)
                ? EntityState.Modified
                : EntityState.Added);

            _dbContext.SaveChanges();
            return entity;
        }

        public void Delete(User entity)
        {
            _dbContext.ValidAdmins.Remove(entity);
            _dbContext.SaveChanges();
        }

        public IEnumerable<User> FindAll()
        {
            return _dbContext.ValidAdmins.ToList();
        }
    }
}