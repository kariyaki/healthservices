﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Nurse
    {
        [Key]
        public long Id { get; set; }

        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}