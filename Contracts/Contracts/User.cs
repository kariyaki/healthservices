﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class User
    {
        [Key]
        public string Email { get; set; }
        public string Password { get; set; }
    }
}