﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    /// <summary>
    /// Consultation
    /// </summary>
    public class Consultation
    {
        /// <summary>
        /// Database Id of the consultation
        /// </summary>
        [Key]
        public long? Id { get; set; }

        /// <summary>
        /// Guid id of the consultation
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// The date of the consultation
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// The patient's preperation to the consultation
        /// </summary>
        public virtual ConsultationPreperation ConsultationPreperation { get; set; }

        /// <summary>
        /// The patient
        /// </summary>
        public virtual Patient Patient { get; set; }

        /// <summary>
        /// The ssn (social security number) to the patient
        /// </summary>
        public string PatientSsn { get; set; }

        /// <summary>
        /// The doctor
        /// </summary>
        public virtual Doctor Doctor { get; set; }

        /// <summary>
        /// The ssn (social security number) to the doctor
        /// </summary>
        public string DoctorSsn { get; set; }
    }
}