﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class SideEffect
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public bool New { get; set; }
        public string IllnessSeverity { get; set; }
    }
}