﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts
{
    public class Symptom
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Severity { get; set; }
        public string Change { get; set; }
        public bool Important { get; set; }
    }
}