﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class SettingsOtherSubject
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
    }
}