﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class GeneralPractitionerOffice
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string Zipcode  { get; set; }
        public string City { get; set; }

        public List<Doctor> Doctors { get; set; }
        public List<Nurse> Nurses { get; set; }
        public List<Patient> Patients { get; set; }
        public List<Consultation> Consultations { get; set; }
    }
}