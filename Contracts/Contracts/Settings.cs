﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Settings
    {
        [Key]
        public long Id { get; set; }

        public int ConsultationPreperationStart { get; set; }
        public List<SettingsSymptom> DefaultSymptoms { get; set; }
        public List<SettingsOtherSubject> DefaultOtherSubjects { get; set; }
    }
}