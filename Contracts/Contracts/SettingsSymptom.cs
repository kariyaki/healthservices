﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class SettingsSymptom
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Severity { get; set; }
        public string Change { get; set; }
        public bool Important { get; set; }
    }
}