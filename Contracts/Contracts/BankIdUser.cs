﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class BankIdUser
    {
        [Key]
        public string Ssn { get; set; }

        public string Password { get; set; }
    }
}