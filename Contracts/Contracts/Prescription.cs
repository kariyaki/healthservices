﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Prescription
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public string DoctorFullName { get; set; }
    }
}