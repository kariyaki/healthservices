﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Doctor
    {
        [Key]
        public long Id { get; set; }

        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Specialization { get; set; }
    }
}