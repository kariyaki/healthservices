﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Department
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public List<Doctor> Doctors { get; set; }
        public List<Nurse> Nurses { get; set; }
    }
}