﻿using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Medicine
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
    }
}