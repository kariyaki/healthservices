﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class ConsultationPreperation
    {
        [Key]
        public long Id { get; set; }

        public Guid Guid { get; set; }
        public DateTime ConsultationTime { get; set; }
        public string NeedForConsultation { get; set; }
        public List<Symptom> Symptoms { get; set; }
        public string SymptomListUpdated { get; set; }
        public string HasSideEffects { get; set; }
        public string NewSideEffectsDegree { get; set; }
        public string OldSideEffectsDegree { get; set; }
        public string SideEffectsNote { get; set; }
        public bool? SideEffectIsImportant { get; set; }
        public string SideEffectsUpdated { get; set; }
        public List<OtherSubject> OtherSubjectsList { get; set; }
        public string OtherSubjectsNote { get; set; }

        public Guid ConsultationGuid { get; set; }
    }
}