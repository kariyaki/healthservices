﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class Patient
    {
        [Key]
        public long Id { get; set; }

        public string Ssn { get; set; }
        public string FirstName  { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Sex { get; set; }
        public DateTime Birthday { get; set; }

        public Doctor Doctor { get; set; }
    }
}