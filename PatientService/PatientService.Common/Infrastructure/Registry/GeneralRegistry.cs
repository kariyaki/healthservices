﻿using log4net;

namespace PatientService.Common.Infrastructure.Registry
{
    public class GeneralRegistry : StructureMap.Registry
    {
        public GeneralRegistry()
        {
            For<ILog>().Use(s => LogManager.GetLogger(s.RootType));

        }
    }
}