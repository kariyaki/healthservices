﻿using System;
using System.Collections.Generic;
using Contracts;
using log4net;
using PatientService.Repository.Repository;

namespace PatientService.BusinessLogic
{
    public class PatientControllerService : IPatientControllerService
    {
        private readonly IPatientRepository _patientRepository;
        private const int ValidSsnLength = 11;
        private readonly ILog _logger;

        public PatientControllerService(IPatientRepository patientRepository, ILog logger)
        {
            if (patientRepository == null) throw new ArgumentNullException(nameof(patientRepository));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _patientRepository = patientRepository;
            _logger = logger;
        }


        private bool ValidSsn(string ssn)
        {
            return ssn.Length == ValidSsnLength;
        }

        private bool ValidGivenName(string givenName)
        {
            return givenName.Length > 0;
        }

        private bool ValidFamilyName(string familyName)
        {
            return familyName.Length > 0;
        }

        private bool ValidSex(string sex)
        {
            return sex.ToLower() == "male" || sex.ToLower() == "female";
        }

        public bool ValidPatient(Patient patient)
        {
            return ValidGivenName(patient.FirstName) && ValidFamilyName(patient.LastName)
                   && ValidSsn(patient.Ssn) && ValidSex(patient.Sex);
        }

        public Patient GetPatient(string ssn)
        {
            return  _patientRepository.Get(ssn);
        }

        public Patient CreatePatient(Patient patient)
        {
            return !ValidPatient(patient) ? null : _patientRepository.Save(patient);
        }

        public Patient UpdatePatient(Patient patient)
        {
            try
            {
                if (patient.Doctor == null)
                {
                    patient.Doctor = _patientRepository.Get(patient.Ssn).Doctor;
                }
                return !ValidPatient(patient) ? null : _patientRepository.Save(patient);
            }
            catch (Exception ex)
            {
                _logger.Error("Error updating patient: " + ex);
                throw;
            }
        }

        public IEnumerable<Patient> GetAllPatients()
        {
            return _patientRepository.FindAll();
        }

        public void DeletePatient(string ssn)
        {
            _patientRepository.Delete(_patientRepository.Get(ssn));
        }
    }
}