﻿using System.Collections.Generic;
using Contracts;

namespace PatientService.BusinessLogic
{
    public interface IPatientControllerService
    {

        /// <summary>
        /// Validates all the fields of the patient to check if patient is valid
        /// </summary>
        /// <param name="patient">Patient to validate</param>
        /// <returns>true if valid patient, false if invalid</returns>
        bool ValidPatient(Patient patient);

        /// <summary>
        /// Tries to retrieve a patient from the DB through the repository
        /// </summary>
        /// <param name="ssn">ssn to the patient</param>
        /// <returns>Patient-object if exists in DB, null if not</returns>
        Patient GetPatient(string ssn);

        /// <summary>
        /// Tries to create a new patient through the repository
        /// </summary>
        /// <param name="patient">new patient</param>
        /// <returns>Patient-object with a ID if success, null if not</returns>
        Patient CreatePatient(Patient patient);

        /// <summary>
        /// Tries to update the patient.
        /// </summary>
        /// <param name="patient">new patient data</param>
        /// <returns>Patient-object if valid patient, null if not</returns>
        Patient UpdatePatient(Patient patient);

        IEnumerable<Patient> GetAllPatients();
        void DeletePatient(string ssn);
    }
}