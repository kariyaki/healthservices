﻿using System;
using System.Web.Http.Results;
using Contracts;
using log4net;
using Moq;
using NUnit.Framework;
using PatientService.API.Controllers;
using PatientService.BusinessLogic;

namespace PatientService.Tests
{
    [TestFixture]
    public class PatientsControllerTest
    {
        private PatientsController _patientController;
        private Mock<IPatientControllerService> _patientServiceMock;
        private Patient _patient;
        private Mock<ILog> _logger;

        [SetUp]
        public void Setup()
        {
            _patientServiceMock = new Mock<IPatientControllerService>();
            _logger = new Mock<ILog>();
            _patient = new Patient
            {
                FirstName = "Ola",
                LastName = "Normann",
                Sex = "Male",
                Ssn = "10028592138",
                Id = 1,
            };
        }

        [Test]
        public void ShouldGetStatuscodeOkWhenGettingPatientWithValidSsn()
        {
            _patientServiceMock.Setup(t => t.GetPatient(_patient.Ssn)).Returns(_patient);
            _patientController = new PatientsController(_patientServiceMock.Object,_logger.Object);

            var httpResult = _patientController.GetPatient(_patient.Ssn) as OkNegotiatedContentResult<Patient>;
            Assert.IsNotNull(httpResult);
            Assert.AreEqual(_patient, httpResult.Content);
        }

        [Test]
        public void ShouldGetStatuscodeNotFoundWhenGettingPatientWithUnknownSsn()
        {
            _patientServiceMock.Setup(t => t.GetPatient(_patient.Ssn)).Returns((Patient)null);
            _patientController = new PatientsController(_patientServiceMock.Object,_logger.Object);

            var httpResult = _patientController.GetPatient(_patient.Ssn) as NotFoundResult;
            Assert.IsNotNull(httpResult);
        }

        [Test]
        public void ShouldGetStatuscodeCreatedWhenCreatingValidPatient()
        {
            _patientServiceMock.Setup(t => t.CreatePatient(_patient)).Returns(_patient);
            _patientController = new PatientsController(_patientServiceMock.Object,_logger.Object);

            var httpResult = _patientController.CreatePatient(_patient) as CreatedNegotiatedContentResult<Patient>;
            Assert.IsNotNull(httpResult);
            Assert.AreEqual(_patient, httpResult.Content);
        }

        [Test]
        public void ShouldGetStatuscodeBadRequestWhenCreatingInvalidPatient()
        {
            _patientServiceMock.Setup(t => t.CreatePatient(_patient)).Returns((Patient)null);
            _patientController = new PatientsController(_patientServiceMock.Object,_logger.Object);

            var httpResult = _patientController.CreatePatient(_patient) as BadRequestResult;
            Assert.IsNotNull(httpResult);
        }

        [Test]
        public void ShouldGetStatuscodeOkWhenUpdatingValidPatient()
        {
            _patientServiceMock.Setup(t => t.UpdatePatient(_patient)).Returns(_patient);
            _patientController = new PatientsController(_patientServiceMock.Object, _logger.Object);

            var httpResult = _patientController.UpdatePatient(_patient) as OkNegotiatedContentResult<Patient>;
            Assert.IsNotNull(httpResult);
            Assert.AreEqual(_patient, httpResult.Content);
        }

        [Test]
        public void ShouldGetStatuscodeBadRequestWhenUpdatingInvalidPatient()
        {
            _patientServiceMock.Setup(t => t.UpdatePatient(_patient)).Returns((Patient)null);
            _patientController = new PatientsController(_patientServiceMock.Object, _logger.Object);

            var httpResult = _patientController.UpdatePatient( _patient) as BadRequestResult;
            Assert.IsNotNull(httpResult);
        }
    }
}

