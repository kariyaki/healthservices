﻿using Contracts;
using log4net;
using Moq;
using NUnit.Framework;
using PatientService.BusinessLogic;
using PatientService.Repository.Repository;

namespace PatientService.Tests
{
    [TestFixture]
    public class PatientServiceTest
    {

        private IPatientControllerService _patientControllerService;
        private Mock<IPatientRepository> _patientRepository;
        private Patient _patient;
        private Mock<ILog> _logger;

        [SetUp]
        public void Setup()
        {
            _patientRepository = new Mock<IPatientRepository>();
            _logger = new Mock<ILog>();
            _patientControllerService = new PatientControllerService(_patientRepository.Object, _logger.Object);

            _patient = new Patient
            {
                FirstName = "Ola",
                LastName = "Normann",
                Sex = "Male",
                Ssn = "10028592138",
                Id = 1,
                Doctor = new Doctor()
            };
        }

        [Test]
        public void ShouldReturnTrueOnValidPatient()
        {
            var result = _patientControllerService.ValidPatient(_patient);
            Assert.IsTrue(result);
        }

        [Test]
        public void ShouldReturnFalseOnInvalidPatientSsn()
        {
            _patient.Ssn = "100586253654";
            var result = _patientControllerService.ValidPatient(_patient);
            Assert.IsFalse(result);
        }

        [Test]
        public void ShouldReturnFalseOnInvalidPatientGivenName()
        {
            _patient.FirstName = "";
            var result = _patientControllerService.ValidPatient(_patient);
            Assert.IsFalse(result);
        }

        [Test]
        public void ShouldReturnFalseOnInvalidPatientFamilyName()
        {
            _patient.LastName = "";
            var result = _patientControllerService.ValidPatient(_patient);
            Assert.IsFalse(result);
        }

        [Test]
        public void ShouldReturnFalseOnInvalidPatientSex()
        {
            _patient.Sex = "";
            var result = _patientControllerService.ValidPatient(_patient);
            Assert.IsFalse(result);
        }

        [Test]
        public void ShouldReturnPatientWhenGettingValidPatient()
        {
            _patientRepository.Setup(t => t.Get(_patient.Ssn)).Returns(_patient);

            var result = _patientControllerService.GetPatient(_patient.Ssn);
            Assert.AreEqual(_patient, result);
        }

        [Test]
        public void ShouldReturnNullWhenGettingInvalidPatient()
        {
            _patientRepository.Setup(t => t.Get(_patient.Ssn)).Returns((Patient)null);

            var result = _patientControllerService.GetPatient(_patient.Ssn);
            Assert.IsNull(result);
        }

        [Test]
        public void ShouldReturnPatientWhenCreatingNewValidPatient()
        {
            _patientRepository.Setup(t => t.Save(_patient)).Returns(_patient);

            var result = _patientControllerService.CreatePatient(_patient);
            Assert.AreEqual(_patient, result);
        }

        [Test]
        public void ShouldReturnNullWhenCreatingInvalidPatient()
        {
            _patientRepository.Setup(t => t.Save(_patient)).Returns((Patient)null);

            var result = _patientControllerService.CreatePatient(_patient);
            Assert.IsNull(result);
        }

        [Test]
        public void ShouldReturnPatientWhenUpdatingValidPatient()
        {
            _patientRepository.Setup(t => t.Save(_patient)).Returns(_patient);

            var result = _patientControllerService.UpdatePatient(_patient);
            Assert.AreEqual(_patient, result);
        }

        [Test]
        public void ShouldReturnNullWhenUpdatingInvalidPatient()
        {
            _patientRepository.Setup(t => t.Save(_patient)).Returns((Patient)null);

            var result = _patientControllerService.UpdatePatient(_patient);
            Assert.IsNull(result);
        }
    }
}
