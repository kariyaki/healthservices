﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using PatientService.API.Infrastructure;
using PatientService.API.Utils;
using PatientService.Common.Infrastructure;

namespace PatientService.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            RegisterStructureMapActivator(config);
            config.Formatters.Add(new BrowserJsonFormatter());
        }

        private static void RegisterStructureMapActivator(HttpConfiguration config)
        {
            config.Services.Replace(
                typeof(IHttpControllerActivator),
                new StructureMapControllerActivator(StructuremapBootstrapper.Initialize()));
        }
    }
}
