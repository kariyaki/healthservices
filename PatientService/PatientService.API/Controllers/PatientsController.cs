﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using Contracts;
using log4net;
using PatientService.BusinessLogic;

namespace PatientService.API.Controllers
{
    /// <summary>
    /// Patients
    /// </summary>
    [RoutePrefix("api/patients")]
    public class PatientsController : ApiController
    {
        private readonly IPatientControllerService _patientService;
        private readonly ILog _logger;

        /// <summary>
        /// Patients constructor
        /// </summary>
        /// <param name="patientService"></param>
        /// <param name="logger"></param>
        public PatientsController(IPatientControllerService patientService, ILog logger)
        {
            if (patientService == null) throw new ArgumentNullException(nameof(patientService));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _patientService = patientService;
            _logger = logger;
        }

        /// <summary>
        /// Get all patients
        /// </summary>
        /// <remarks>
        /// Get all patients
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<Patient> GetAllPatients()
        {
            _logger.Debug("GetAllPatients");
            var all = _patientService.GetAllPatients();
            return all;
        }

        /// <summary>
        /// Get patient
        /// </summary>
        /// <remarks>
        /// Get patient with specified ssn (Social security number)
        /// </remarks>
        /// <param name="patientssn"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Patient))]
        [Route("{patientssn}")]
        public IHttpActionResult GetPatient(string patientssn)
        {
            _logger.Debug("GetPatient");

            var patient = _patientService.GetPatient(patientssn);

            if (patient != null) return Ok(patient);
            return NotFound();
        }

        /// <summary>
        /// Create patient
        /// </summary>
        /// <remarks>
        /// Create patient with specified patient data
        /// </remarks>
        /// <param name="patient"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(Patient))]
        [Route("")]
        public IHttpActionResult CreatePatient([FromBody]Patient patient)
        {
            _logger.Debug("Create patient");

            var newPatient = _patientService.CreatePatient(patient);

            if (newPatient == null) return BadRequest();

            return Created("", newPatient);
        }

        /// <summary>
        /// Update patient
        /// </summary>
        /// <remarks>
        /// Update patient with specified patient data
        /// </remarks>
        /// <param name="patient"></param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(Patient))]
        [Route("")]
        public IHttpActionResult UpdatePatient([FromBody] Patient patient)
        {
            _logger.Debug("Update patient");
            var updatedPatient = _patientService.UpdatePatient(patient);

            if (updatedPatient != null) return Ok(updatedPatient);
            return BadRequest();
        }

        [HttpDelete]
        [Route("{ssn}")]
        [Obsolete]
        public void DeletePatient(string ssn)
        {
            _patientService.DeletePatient(ssn);
        }
    }
}