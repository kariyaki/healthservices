namespace Service.Repository.Migrations.PatientServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SympOthRelaFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.Symptom", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation");
            DropIndex("dbo.OtherSubject", new[] { "ConsultationPreperation_Id1" });
            DropIndex("dbo.Symptom", new[] { "ConsultationPreperation_Id1" });
            DropColumn("dbo.OtherSubject", "Id");
            DropColumn("dbo.Symptom", "Id");
            RenameColumn(table: "dbo.OtherSubject", name: "ConsultationPreperation_Id1", newName: "Id");
            RenameColumn(table: "dbo.Symptom", name: "ConsultationPreperation_Id1", newName: "Id");
            DropPrimaryKey("dbo.OtherSubject");
            DropPrimaryKey("dbo.Symptom");
            AlterColumn("dbo.OtherSubject", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Symptom", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.OtherSubject", "Id");
            AddPrimaryKey("dbo.Symptom", "Id");
            CreateIndex("dbo.OtherSubject", "Id");
            CreateIndex("dbo.Symptom", "Id");
            AddForeignKey("dbo.OtherSubject", "Id", "dbo.ConsultationPreperation", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Symptom", "Id", "dbo.ConsultationPreperation", "Id", cascadeDelete: true);
            DropColumn("dbo.OtherSubject", "ConsultationPreperation_Id");
            DropColumn("dbo.Symptom", "ConsultationPreperation_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Symptom", "ConsultationPreperation_Id", c => c.Long(nullable: false));
            AddColumn("dbo.OtherSubject", "ConsultationPreperation_Id", c => c.Long(nullable: false));
            DropForeignKey("dbo.Symptom", "Id", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.OtherSubject", "Id", "dbo.ConsultationPreperation");
            DropIndex("dbo.Symptom", new[] { "Id" });
            DropIndex("dbo.OtherSubject", new[] { "Id" });
            DropPrimaryKey("dbo.Symptom");
            DropPrimaryKey("dbo.OtherSubject");
            AlterColumn("dbo.Symptom", "Id", c => c.Long());
            AlterColumn("dbo.OtherSubject", "Id", c => c.Long());
            AddPrimaryKey("dbo.Symptom", "Id");
            AddPrimaryKey("dbo.OtherSubject", "Id");
            RenameColumn(table: "dbo.Symptom", name: "Id", newName: "ConsultationPreperation_Id1");
            RenameColumn(table: "dbo.OtherSubject", name: "Id", newName: "ConsultationPreperation_Id1");
            AddColumn("dbo.Symptom", "Id", c => c.Long(nullable: false, identity: true));
            AddColumn("dbo.OtherSubject", "Id", c => c.Long(nullable: false, identity: true));
            CreateIndex("dbo.Symptom", "ConsultationPreperation_Id1");
            CreateIndex("dbo.OtherSubject", "ConsultationPreperation_Id1");
            AddForeignKey("dbo.Symptom", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation", "Id");
            AddForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation", "Id");
        }
    }
}
