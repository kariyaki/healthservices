using System.Data.Entity.Migrations;

namespace PatientService.Repository.Migrations.PatientServiceContext
{
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConsultationPreperation",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Guid = c.Guid(nullable: false),
                        ConsultationTime = c.DateTime(nullable: false),
                        NeedForConsultation = c.String(),
                        SymptomListUpdated = c.String(),
                        HasSideEffects = c.String(),
                        NewSideEffectsDegree = c.String(),
                        OldSideEffectsDegree = c.String(),
                        SideEffectsNote = c.String(),
                        SideEffectIsImportant = c.Boolean(),
                        SideEffectsUpdated = c.String(),
                        OtherSubjectsNote = c.String(),
                        ConsultationGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OtherSubject",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        ConsultationPreperation_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConsultationPreperation", t => t.ConsultationPreperation_Id)
                .Index(t => t.ConsultationPreperation_Id);
            
            CreateTable(
                "dbo.Symptom",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Severity = c.String(),
                        Change = c.String(),
                        Important = c.Boolean(nullable: false),
                        ConsultationPreperation_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConsultationPreperation", t => t.ConsultationPreperation_Id)
                .Index(t => t.ConsultationPreperation_Id);
            
            CreateTable(
                "dbo.Consultation",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Guid = c.Guid(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        PatientSsn = c.String(),
                        DoctorSsn = c.String(),
                        ConsultationPreperation_Id = c.Long(),
                        GeneralPractitionerOffice_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConsultationPreperation", t => t.ConsultationPreperation_Id)
                .ForeignKey("dbo.GeneralPractitionerOffice", t => t.GeneralPractitionerOffice_Id)
                .Index(t => t.ConsultationPreperation_Id)
                .Index(t => t.GeneralPractitionerOffice_Id);
            
            CreateTable(
                "dbo.Doctor",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Ssn = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Specialization = c.String(),
                        GeneralPractitionerOffice_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeneralPractitionerOffice", t => t.GeneralPractitionerOffice_Id)
                .Index(t => t.GeneralPractitionerOffice_Id);
            
            CreateTable(
                "dbo.Patient",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Ssn = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        Zipcode = c.String(),
                        City = c.String(),
                        Sex = c.String(),
                        Birthday = c.DateTime(nullable: false),
                        Doctor_Id = c.Long(),
                        GeneralPractitionerOffice_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctor", t => t.Doctor_Id)
                .ForeignKey("dbo.GeneralPractitionerOffice", t => t.GeneralPractitionerOffice_Id)
                .Index(t => t.Doctor_Id)
                .Index(t => t.GeneralPractitionerOffice_Id);
            
            CreateTable(
                "dbo.GeneralPractitionerOffice",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Zipcode = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Nurse",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Ssn = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        GeneralPractitionerOffice_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeneralPractitionerOffice", t => t.GeneralPractitionerOffice_Id)
                .Index(t => t.GeneralPractitionerOffice_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patient", "GeneralPractitionerOffice_Id", "dbo.GeneralPractitionerOffice");
            DropForeignKey("dbo.Nurse", "GeneralPractitionerOffice_Id", "dbo.GeneralPractitionerOffice");
            DropForeignKey("dbo.Doctor", "GeneralPractitionerOffice_Id", "dbo.GeneralPractitionerOffice");
            DropForeignKey("dbo.Consultation", "GeneralPractitionerOffice_Id", "dbo.GeneralPractitionerOffice");
            DropForeignKey("dbo.Patient", "Doctor_Id", "dbo.Doctor");
            DropForeignKey("dbo.Consultation", "ConsultationPreperation_Id", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.Symptom", "ConsultationPreperation_Id", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id", "dbo.ConsultationPreperation");
            DropIndex("dbo.Nurse", new[] { "GeneralPractitionerOffice_Id" });
            DropIndex("dbo.Patient", new[] { "GeneralPractitionerOffice_Id" });
            DropIndex("dbo.Patient", new[] { "Doctor_Id" });
            DropIndex("dbo.Doctor", new[] { "GeneralPractitionerOffice_Id" });
            DropIndex("dbo.Consultation", new[] { "GeneralPractitionerOffice_Id" });
            DropIndex("dbo.Consultation", new[] { "ConsultationPreperation_Id" });
            DropIndex("dbo.Symptom", new[] { "ConsultationPreperation_Id" });
            DropIndex("dbo.OtherSubject", new[] { "ConsultationPreperation_Id" });
            DropTable("dbo.Nurse");
            DropTable("dbo.GeneralPractitionerOffice");
            DropTable("dbo.Patient");
            DropTable("dbo.Doctor");
            DropTable("dbo.Consultation");
            DropTable("dbo.Symptom");
            DropTable("dbo.OtherSubject");
            DropTable("dbo.ConsultationPreperation");
        }
    }
}
