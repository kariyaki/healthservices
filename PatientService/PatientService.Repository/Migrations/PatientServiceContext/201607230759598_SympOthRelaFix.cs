namespace Service.Repository.Migrations.PatientServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SympOthRelaFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.Symptom", "ConsultationPreperation_Id", "dbo.ConsultationPreperation");
            DropIndex("dbo.OtherSubject", new[] { "ConsultationPreperation_Id" });
            DropIndex("dbo.Symptom", new[] { "ConsultationPreperation_Id" });
            AddColumn("dbo.OtherSubject", "ConsultationPreperation_Id1", c => c.Long());
            AddColumn("dbo.Symptom", "ConsultationPreperation_Id1", c => c.Long());
            AlterColumn("dbo.OtherSubject", "ConsultationPreperation_Id", c => c.Long(nullable: false));
            AlterColumn("dbo.Symptom", "ConsultationPreperation_Id", c => c.Long(nullable: false));
            CreateIndex("dbo.OtherSubject", "ConsultationPreperation_Id1");
            CreateIndex("dbo.Symptom", "ConsultationPreperation_Id1");
            AddForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation", "Id");
            AddForeignKey("dbo.Symptom", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Symptom", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation");
            DropForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id1", "dbo.ConsultationPreperation");
            DropIndex("dbo.Symptom", new[] { "ConsultationPreperation_Id1" });
            DropIndex("dbo.OtherSubject", new[] { "ConsultationPreperation_Id1" });
            AlterColumn("dbo.Symptom", "ConsultationPreperation_Id", c => c.Long());
            AlterColumn("dbo.OtherSubject", "ConsultationPreperation_Id", c => c.Long());
            DropColumn("dbo.Symptom", "ConsultationPreperation_Id1");
            DropColumn("dbo.OtherSubject", "ConsultationPreperation_Id1");
            CreateIndex("dbo.Symptom", "ConsultationPreperation_Id");
            CreateIndex("dbo.OtherSubject", "ConsultationPreperation_Id");
            AddForeignKey("dbo.Symptom", "ConsultationPreperation_Id", "dbo.ConsultationPreperation", "Id");
            AddForeignKey("dbo.OtherSubject", "ConsultationPreperation_Id", "dbo.ConsultationPreperation", "Id");
        }
    }
}
