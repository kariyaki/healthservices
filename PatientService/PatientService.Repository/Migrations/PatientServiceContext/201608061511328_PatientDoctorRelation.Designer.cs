// <auto-generated />

using System.CodeDom.Compiler;
using System.Data.Entity.Migrations.Infrastructure;
using System.Resources;

namespace PatientService.Repository.Migrations.PatientServiceContext
{
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PatientDoctorRelation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PatientDoctorRelation));
        
        string IMigrationMetadata.Id
        {
            get { return "201608061511328_PatientDoctorRelation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
