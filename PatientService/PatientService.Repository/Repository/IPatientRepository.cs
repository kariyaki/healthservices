﻿using Contracts;

namespace PatientService.Repository.Repository
{
    public interface IPatientRepository : IRepository<Patient, string>
    {
    }
}