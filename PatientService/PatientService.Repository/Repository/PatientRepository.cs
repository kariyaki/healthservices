﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts;
using log4net;
using Newtonsoft.Json;
using PatientService.Repository.DAL;

namespace PatientService.Repository.Repository
{
    public class PatientRepository : IPatientRepository
    {
        private readonly PatientServiceContext _dbContext;
        private readonly ILog _logger;

        public PatientRepository(PatientServiceContext dbContext, ILog logger)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _dbContext = dbContext;
            _logger = logger;
        }

        public Patient Get(string patientssn)
        {
            var patient = _dbContext.Patients
                .Include(t => t.Doctor)
                .FirstOrDefault(t => t.Ssn == patientssn);
            return patient;
        }

        public Patient Save(Patient entity)
        {
            _logger.Debug(JsonConvert.SerializeObject(entity));
            var old = Get(entity.Ssn);

            if (old != null)
            {
                _dbContext.Entry(old).CurrentValues.SetValues(entity);
            }
            else
            {
                _dbContext.Entry(entity).State = EntityState.Added;
                _dbContext.BankIdUsers.Add(new BankIdUser {Ssn = entity.Ssn, Password = "Password"});
            }

            _dbContext.SaveChanges();
            return entity;
        }

        public IEnumerable<Patient> FindAll()
        {
            return _dbContext.Patients
                .Include(t => t.Doctor)
                .ToList();
        }

        public void Delete(Patient entity)
        {
            _dbContext.Patients.Remove(entity);
            _dbContext.BankIdUsers.Remove(_dbContext.BankIdUsers.Find(entity.Ssn));
            _dbContext.SaveChanges();
        }
    }
}