﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Contracts;

namespace ConsultationService.Repository.DAL
{
    public class ConsultationServiceContext : BaseContext<ConsultationServiceContext>
    {
        public DbSet<Consultation> Consultations { get; set; }
        public DbSet<Symptom> Symptoms { get; set; }
        public DbSet<ConsultationPreperation> ConsultationPreperations { get; set; }
        public DbSet<OtherSubject> OtherSubjects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Consultation>()
                .HasOptional(s => s.ConsultationPreperation);

            modelBuilder.Entity<Consultation>()
                .HasOptional(s => s.Doctor);

            modelBuilder.Entity<Consultation>()
                .HasOptional(s => s.Patient);
        }
    }
}