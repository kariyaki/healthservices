﻿using System.Data.Entity;

namespace ConsultationService.Repository.DAL
{
    public class BaseContext<TContext> : DbContext where TContext: DbContext
    {
        static BaseContext()
        {
            Database.SetInitializer<TContext>(null);
        }

        protected BaseContext()
            : base("name=HealthDatabase")
        {
        }
    }
}