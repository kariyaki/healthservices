﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ConsultationService.Repository.DAL;
using Contracts;
using log4net;
using Newtonsoft.Json;

namespace ConsultationService.Repository.Repository
{
    public class ConsultationPreperationRepository : IConsultationPreperationRepository
    {
        private readonly ConsultationServiceContext _dbContext;
        private readonly ILog _logger = LogManager.GetLogger(typeof(ConsultationPreperationRepository));

        public ConsultationPreperationRepository(ConsultationServiceContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ConsultationPreperation Get(Guid guid)
        {
            var consultationpreperation = _dbContext.ConsultationPreperations
                .Include(a => a.Symptoms)
                .Include(a => a.OtherSubjectsList)
                .FirstOrDefault(t => t.Guid == guid);
            return consultationpreperation;
        }

        public ConsultationPreperation Save(ConsultationPreperation entity)
        {
            _logger.Debug("Saving ConsultationPreperation:" + JsonConvert.SerializeObject(entity));
            var old = Get(entity.Guid);

            if (old != null)
            {
                HandleLists(entity, old);
                _dbContext.Entry(old).CurrentValues.SetValues(entity);
                old.Symptoms = new List<Symptom>(entity.Symptoms);
                old.OtherSubjectsList = new List<OtherSubject>(entity.OtherSubjectsList);
            }
            else
            {
                _dbContext.Entry(entity).State = EntityState.Added;
            }
            _logger.Debug("Saving...");
            _dbContext.SaveChanges();
            return entity;
        }

        private void HandleLists(ConsultationPreperation entity, ConsultationPreperation oldCopy)
        {
            _logger.Debug("old: " + JsonConvert.SerializeObject(oldCopy));
            _logger.Debug("new: " + JsonConvert.SerializeObject(entity));
            var remSymp = new List<Symptom>();
            var addSymp = new List<Symptom>();
            if (entity.Symptoms != null)
            {
                if (oldCopy.Symptoms != null)
                {
                    remSymp.AddRange(oldCopy.Symptoms.Except(entity.Symptoms));

                    addSymp.AddRange(entity.Symptoms.Except(oldCopy.Symptoms));
                }
                else
                {
                    addSymp.AddRange(entity.Symptoms);
                }
            }

            foreach (var symptom in remSymp)
            {
                _dbContext.Symptoms.Remove(symptom);
            }

            foreach (var symptom in addSymp)
            {
                _dbContext.Symptoms.Add(symptom);
            }

            var remOth = new List<OtherSubject>();
            var addOth = new List<OtherSubject>();
            if (entity.OtherSubjectsList != null)
            {
                if (oldCopy.OtherSubjectsList != null)
                {
                    remOth.AddRange(oldCopy.OtherSubjectsList.Except(entity.OtherSubjectsList));

                    addOth.AddRange(entity.OtherSubjectsList.Except(oldCopy.OtherSubjectsList));
                }
                else
                {
                    addOth.AddRange(entity.OtherSubjectsList);
                }
            }

            foreach (var oth in remOth)
            {
                _dbContext.OtherSubjects.Remove(oth);
            }

            foreach (var oth in addOth)
            {
                _dbContext.OtherSubjects.Add(oth);
            }
        }

        public IEnumerable<ConsultationPreperation> FindAll()
        {
            return _dbContext.ConsultationPreperations
                .Include(a => a.OtherSubjectsList)
                .Include(a => a.Symptoms)
                .ToList();
        }

        public void Delete(ConsultationPreperation entity)
        {
            _dbContext.ConsultationPreperations.Remove(entity);

            foreach (var symptom in entity.Symptoms)
            {
                _dbContext.Symptoms.Remove(symptom);
            }

            foreach (var other in entity.OtherSubjectsList)
            {
                _dbContext.OtherSubjects.Remove(other);
            }
            _dbContext.SaveChanges();
        }
    }
}