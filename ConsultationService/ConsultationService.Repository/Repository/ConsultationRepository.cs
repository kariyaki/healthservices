﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ConsultationService.Repository.DAL;
using Contracts;
using log4net;
using Newtonsoft.Json;

namespace ConsultationService.Repository.Repository
{
    public class ConsultationRepository : IConsultationRepository
    {
        private readonly ConsultationServiceContext _dbContext;
        private readonly ILog _logger = LogManager.GetLogger(typeof(ConsultationRepository));
        private readonly IConsultationPreperationRepository _consultationPreperationRepository;

        public ConsultationRepository(ConsultationServiceContext dbContext, IConsultationPreperationRepository consultationPreperationRepository)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            if (consultationPreperationRepository == null)
                throw new ArgumentNullException(nameof(consultationPreperationRepository));
            _dbContext = dbContext;
            _consultationPreperationRepository = consultationPreperationRepository;
        }

        public Consultation Get(Guid guid)
        {
            var consultation = _dbContext.Consultations
                .Include(a => a.Patient)
                .Include(a => a.Doctor)
                .Include(a => a.ConsultationPreperation.Symptoms)
                .Include(a => a.ConsultationPreperation.OtherSubjectsList)
                .FirstOrDefault(t => t.Guid == guid);
            return consultation;
        }

        public Consultation Save(Consultation entity)
        {
            _logger.Debug("Saving Consultation:" + JsonConvert.SerializeObject(entity));
            var old = Get(entity.Guid);

            if (old != null)
            {
                _logger.Debug("old: " + JsonConvert.SerializeObject(old));
                _dbContext.Entry(old).CurrentValues.SetValues(entity);
                _dbContext.Entry(old.ConsultationPreperation).CurrentValues.SetValues(entity.ConsultationPreperation);

                if (entity.Doctor.Id != old.Doctor.Id)
                {
                    _dbContext.Entry(entity.Doctor).State = EntityState.Modified;
                    old.Doctor = entity.Doctor;
                }
            }
            else
            {
                _logger.Debug("creating...");

                _dbContext.Entry(entity.Patient).State = EntityState.Modified;
                _dbContext.Entry(entity.Doctor).State = EntityState.Modified;
                _dbContext.Entry(entity).State = EntityState.Added;
            }
            _logger.Debug("Saving...");
            _dbContext.SaveChanges();
            return entity;
        }

        public IEnumerable<Consultation> FindAll()
        {
            return _dbContext.Consultations
                .Include(a => a.Patient)
                .Include(a => a.Doctor)
                .Include(a => a.ConsultationPreperation.Symptoms)
                .Include(a => a.ConsultationPreperation.OtherSubjectsList)
                .OrderByDescending(t => t.DateTime)
                .ToList();
        }

        public void Delete(Consultation entity)
        {
            _consultationPreperationRepository.Delete(entity.ConsultationPreperation);
            _dbContext.Consultations.Remove(entity);
            _dbContext.SaveChanges();
        }
    }
}