﻿using System;
using Contracts;

namespace ConsultationService.Repository.Repository
{
    public interface IConsultationPreperationRepository : IRepository<ConsultationPreperation, Guid>
    {
    }
}