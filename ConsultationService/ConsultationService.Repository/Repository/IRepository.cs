﻿using System.Collections.Generic;

namespace ConsultationService.Repository.Repository
{
    public interface IRepository<TEntity, in TKey> where TEntity : class
    {
        TEntity Get(TKey id);
        TEntity Save(TEntity entity);
        IEnumerable<TEntity> FindAll();
        void Delete(TEntity entity);
    }
}