﻿using System;
using Contracts;

namespace ConsultationService.Repository.Repository
{
    public interface IConsultationRepository : IRepository<Consultation, Guid>
    {
    }
}