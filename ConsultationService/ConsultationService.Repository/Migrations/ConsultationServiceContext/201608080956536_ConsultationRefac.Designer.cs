// <auto-generated />
namespace ConsultationService.Repository.Migrations.ConsultationServiceContext
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ConsultationRefac : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ConsultationRefac));
        
        string IMigrationMetadata.Id
        {
            get { return "201608080956536_ConsultationRefac"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
