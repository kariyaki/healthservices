namespace ConsultationService.Repository.Migrations.ConsultationServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConsultationRefac : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Consultation", "Doctor_Id", c => c.Long());
            AddColumn("dbo.Consultation", "Patient_Id", c => c.Long());
            CreateIndex("dbo.Consultation", "Doctor_Id");
            CreateIndex("dbo.Consultation", "Patient_Id");
            AddForeignKey("dbo.Consultation", "Doctor_Id", "dbo.Doctor", "Id");
            AddForeignKey("dbo.Consultation", "Patient_Id", "dbo.Patient", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Consultation", "Patient_Id", "dbo.Patient");
            DropForeignKey("dbo.Consultation", "Doctor_Id", "dbo.Doctor");
            DropIndex("dbo.Consultation", "Doctor_Id");
            DropIndex("dbo.Consultation", "Patient_Id");
            DropColumn("dbo.Consultation", "Doctor_Id");
            DropColumn("dbo.Consultation", "Patient_Id");
        }
    }
}
