using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using Contracts;

namespace ConsultationService.Repository.Migrations.ConsultationServiceContext
{
    internal sealed class Configuration : DbMigrationsConfiguration<DAL.ConsultationServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            MigrationsDirectory = @"Migrations\ConsultationServiceContext";
        }

        protected override void Seed(DAL.ConsultationServiceContext context)
        {
            var consultationGuid = Guid.NewGuid();

            var doctor = new Doctor
            {
                Id = 1,
                Ssn = "12105022456",
                FirstName = "John",
                LastName = "Johnson",
                Specialization = "Allmenn",
            };

            var patient = new Patient
            {
                Id = 1,
                Ssn = "101112131415",
                FirstName = "Ole",
                LastName = "Normann",
                Address = "Adresse 1",
                Zipcode = "5058",
                City = "Bergen",
                Sex = "Male",
                Birthday = new DateTime(2012, 11, 10),
                Doctor = doctor
            };

            var consultation = new Consultation
            {
                Id = 1,
                Guid = consultationGuid,
                DateTime = new DateTime(2016, 08, 05, 11, 30, 00),
                Doctor = doctor,
                DoctorSsn = doctor.Ssn,
                PatientSsn = patient.Ssn,
                Patient = patient,
                ConsultationPreperation = new ConsultationPreperation
                {
                    Id = 1,
                    Guid = Guid.NewGuid(),
                    ConsultationTime = new DateTime(2016, 08, 01, 11, 30, 00),
                    NeedForConsultation = "Ja:Fordi jeg �nsker kontroll",
                    ConsultationGuid = consultationGuid,
                    Symptoms = new List<Symptom>
                    {
                        new Symptom
                        {
                            Id = 1,
                            Name = "Lammelse",
                            Change = "Er blitt v�rre i det siste",
                            Description = "Lammelse betyr.... kort men mer utfyllende forklaring",
                            Severity = "Mye ubehag/p�virker hverdagen i stor grad",
                            Important = true
                        }
                    },
                    SymptomListUpdated = "Ja",
                    HasSideEffects = "Ja",
                    NewSideEffectsDegree = "Sterkt plagsomme",
                    OldSideEffectsDegree = "Moderate",
                    SideEffectsNote = "Note note note",
                    SideEffectIsImportant = true,
                    SideEffectsUpdated = "Nei, se bort fra symptomregistreringen",
                    OtherSubjectsList = new List<OtherSubject>
                    {
                        new OtherSubject { Id = 1, Name= "Sp�rsm�l om MS-medisin"},
                        new OtherSubject { Id = 2, Name=  "�nsker informasjon/avgi samtykke til Norsk MS register og biobank"}
                    },
                    OtherSubjectsNote = "Note note note note"
                }
            };
            context.Consultations.AddOrUpdate(t => t.Id, consultation);
        }
    }
}
