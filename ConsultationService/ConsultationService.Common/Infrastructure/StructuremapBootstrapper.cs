﻿using StructureMap;
using StructureMap.Graph;

namespace ConsultationService.Common.Infrastructure
{
    public class StructuremapBoostrapper
    {
        public static IContainer Initialize()
        {
            return new Container(x =>
                x.Scan(scan =>
                {
                    scan.LookForRegistries();
                    scan.TheCallingAssembly();
                    scan.AssembliesFromApplicationBaseDirectory(y => y.FullName.Contains("Service"));
                    scan.WithDefaultConventions();
                }));
        }
    }
}