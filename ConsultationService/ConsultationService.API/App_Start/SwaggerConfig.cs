using System.Web.Http;
using WebActivatorEx;
using ConsultationService.API;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace ConsultationService.API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration 
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "ConsultationService.API");
                        c.IncludeXmlComments(string.Format(@"{0}\bin\ConsultationService.API.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IgnoreObsoleteActions();
                    })
                .EnableSwaggerUi(c =>
                    {
                       
                    });
        }
    }
}
