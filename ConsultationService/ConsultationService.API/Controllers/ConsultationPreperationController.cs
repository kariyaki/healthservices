﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ConsultationService.BusinessLogic;
using Contracts;
using log4net;

namespace ConsultationService.API.Controllers
{
    /// <summary>
    /// ConsultationPreperation
    /// </summary>
    [RoutePrefix("api/preperation")]
    public class ConsultationPreperationController : ApiController
    {
        private readonly ILog _logger;
        private readonly IConsultationPreperationService _consultationPreperationService;

        /// <summary>
        /// ConsultationPreperation constructor
        /// </summary>
        /// <param name="log">Logging object</param>
        /// <param name="consultationPreperationService">Business logic service</param>
        public ConsultationPreperationController(ILog log, IConsultationPreperationService consultationPreperationService)
        {
            if (log == null) throw new ArgumentNullException(nameof(log));
            if (consultationPreperationService == null)
                throw new ArgumentNullException(nameof(consultationPreperationService));
            _logger = log;
            _consultationPreperationService = consultationPreperationService;
            _logger.Debug("ConsultationPreperation constructor");
        }

        /// <summary>
        /// Get consultation preperation 
        /// </summary>
        /// <remarks>
        /// Get the consultation preperation with specified Guid id
        /// </remarks>
        /// <param name="consultationPreperationGuid"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType((typeof(ConsultationPreperation)))]
        [Route("{consultationPreperationGuid}")]
        public IHttpActionResult GetConsultationPreperation(Guid consultationPreperationGuid)
        {
            _logger.Debug("GetConsultationPreperation");
            try
            {
                var consultationPreperation = _consultationPreperationService.GetConsultationPreperation(consultationPreperationGuid);
                return Ok(consultationPreperation);
            }
            catch (Exception ex)
            {
                _logger.Error("Error getting consultation preperation with guid: " + consultationPreperationGuid + ". " + ex);
                return NotFound();
            }
        }

        /// <summary>
        /// Get all consultation preperations
        /// </summary>
        /// <remarks>
        /// Get all the preperations to the patient with specified ssn (social security number)
        /// </remarks>
        /// <param name="patientssn"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<ConsultationPreperation>))]
        [Route("{patientssn}/all")]
        public IHttpActionResult GetAllConsultationPreperations(string patientssn)
        {
            _logger.Debug("GetConsultationPreperation");
            try
            {
                var consultationPreperation = _consultationPreperationService.GetAllConsultationPreperations(patientssn);
                return Ok(consultationPreperation);
            }
            catch (Exception ex)
            {
                _logger.Error("Error getting consultation preperation with ids: " + patientssn + ". " + ex);
                return NotFound();
            }
        }

        /// <summary>
        /// Get next consultation preperation
        /// </summary>
        /// <remarks>
        /// Gets the next preperation object to the patient with specified ssn (social security number)
        /// </remarks>
        /// <param name="patientssn"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(ConsultationPreperation))]
        [Route("{patientssn}/next")]
        public IHttpActionResult GetNextConsultationPreperation(string patientssn)
        {
            _logger.Debug("GetNextConsultationPreperation");
            try
            {
                var nextConsultationPreperation =
                    _consultationPreperationService.GetNextConsultationPreperation(patientssn);
                return Ok(nextConsultationPreperation);
            }
            catch (Exception ex)
            {
                _logger.Error("Error getting next consultation with patientid: " + patientssn + ". " + ex);
                return NotFound();
            }
        }

        /// <summary>
        /// Create consultation preperation
        /// </summary>
        /// <remarks>
        /// Creates a new preperation with the specified data to the consultation with specified Guid id. 
        /// Guid, Id and ConsultationTime will be set automatically in the preperation object.
        /// </remarks>
        /// <param name="consultationPreperation"></param>
        /// <param name="consultationGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(ConsultationPreperation))]
        [Route("{consultationGuid}")]
        public IHttpActionResult CreateConsultationPreperation([FromBody] ConsultationPreperation consultationPreperation, Guid consultationGuid)
        {
            _logger.Debug("CreateConsultationPreperation");
            try
            {
                var created = _consultationPreperationService.CreateConsultationPreperation(consultationPreperation, consultationGuid);
                return Ok(created);
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating consultation preperation with ids: " + consultationGuid + ". " + ex);
                return BadRequest("Error creating consultation preperation with ids: " + consultationGuid + ". " + ex);
            }
        }

        /// <summary>
        /// Update consultation preperation
        /// </summary>
        /// <remarks>
        /// Updates the consultation preperation with the specified data
        /// </remarks>
        /// <param name="consultationPreperation"></param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(ConsultationPreperation))]
        [Route("")]
        public IHttpActionResult UpdateConsultationPreperation([FromBody] ConsultationPreperation consultationPreperation)
        {
            _logger.Debug("UpdateConsultationPreperation");
            try
            {
                var updated = _consultationPreperationService.UpdateConsultationPreperation(consultationPreperation);
                return Ok(updated);
            }
            catch (Exception ex)
            {
                _logger.Error("Error updating consultation preperation: " + ex);
                return BadRequest("Error updating consultation preperation: " + ex);
            }
        }
    }
}