﻿using System;
using System.Linq;
using System.Web.Http;
using ConsultationService.BusinessLogic;
using Contracts;
using log4net;
using System.Web.Http.Description;
using System.Collections.Generic;

namespace ConsultationService.API.Controllers
{
    /// <summary>
    /// Consultation
    /// </summary>
    [RoutePrefix("api/consultation")]
    public class ConsultationController : ApiController
    {

        private readonly IConsultationService _consultationService;
        private readonly ILog _logger;

        /// <summary>
        /// Consultation constructor
        /// </summary>
        /// <param name="consultationService">Business logic service</param>
        /// <param name="logger">Logging object</param>
        public ConsultationController(IConsultationService consultationService, ILog logger)
        {
            if (consultationService == null) throw new ArgumentNullException(nameof(consultationService));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _consultationService = consultationService;
            _logger = logger;
            _logger.Debug("ConsultationController");
        }

        /// <summary>
        /// Get all consultations
        /// </summary>
        /// <remarks>
        /// Get a list of all consultations
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Consultation>))]
        [Route("")]
        public IHttpActionResult GetAllConsultations()
        {
            _logger.Debug("GetAllConsultations");
            try
            {
                var consultations = _consultationService.GetAllConsultations();
                return Ok(consultations);

            }
            catch (Exception ex)
            {
                _logger.Error("Exception getting all consultations: " + ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get consultation
        /// </summary>
        /// <remarks>
        /// Get a consultation with specified Guid id
        /// </remarks>
        /// <param name="consultationGuid"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [ResponseType(typeof(Consultation))]
        [Route("{consultationGuid}")]
        public IHttpActionResult GetConsultation(Guid consultationGuid)
        {
            _logger.Debug("GetConsultation");
            try
            {
                var consultation = _consultationService.GetConsultation(consultationGuid);
                return Ok(consultation);

            }
            catch (Exception ex)
            {
                _logger.Error("Exception getting all consultation: " + ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Get all consultation for patient
        /// </summary>
        /// <remarks>
        /// Get all consultations for patient with specified ssn</remarks>
        /// <param name="patientSsn"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Consultation>))]
        [Route("{patientSsn}/all")]
        public IHttpActionResult GetAllConsultationsForPatient(string patientSsn)
        {
            _logger.Debug("GetAllConsultationsForPatient");
            try
            {
                var consultations = _consultationService.GetAllConsultations().Where(t => t.Patient.Ssn == patientSsn);
                return Ok(consultations);

            }
            catch (Exception ex)
            {
                _logger.Error("Exception getting all consultations for patient: " + ex + ", patientssn " + patientSsn);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Create consultation
        /// </summary>
        /// <remarks>
        /// Create consultation with specified consultation data
        /// </remarks>
        /// <param name="consultation"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPost]
        [ResponseType(typeof(Consultation))]
        [Route("create")]
        public IHttpActionResult CreateConsultation([FromBody] Consultation consultation)
        {
            _logger.Debug("CreateConsultation");
            try
            {
                var createdConsultation = _consultationService.CreateConsultation(consultation);
                return Ok(createdConsultation);
            }
            catch (Exception ex)
            {
                _logger.Error("Exception creating consultation: " + ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Update consultation
        /// </summary>
        /// <remarks>
        /// Update consultation with specified consultation data
        /// </remarks>
        /// <param name="consultation"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPut]
        [ResponseType(typeof(Consultation))]
        [Route("")]
        public IHttpActionResult UpdateConsultation([FromBody] Consultation consultation)
        {
            _logger.Debug("UpdateConsultation");
            try
            {
                if (consultation.ConsultationPreperation == null)
                {
                    consultation.ConsultationPreperation =
                        _consultationService.GetConsultation(consultation.Guid).ConsultationPreperation;
                }

                if (consultation.Doctor == null)
                {
                    consultation.Doctor =
                        _consultationService.GetConsultation(consultation.Guid).Doctor;
                }

                if (consultation.Patient == null)
                {
                    consultation.Patient =
                        _consultationService.GetConsultation(consultation.Guid).Patient;
                }
                var updatedConsultation = _consultationService.UpdateConsultation(consultation);
                return Ok(updatedConsultation);
            }
            catch (Exception ex)
            {
                _logger.Error("Exception updating consultation: " + ex);
                return InternalServerError(ex);
            }
        }

        [HttpDelete]
        [Route("delete/{consultationGuid}")]
        [Obsolete]
        public void DeleteConsultation(Guid consultationGuid)
        {
            _consultationService.DeleteConsultation(consultationGuid);
        }
    }
}
