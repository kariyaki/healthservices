﻿using System;
using System.Collections.Generic;
using Contracts;

namespace ConsultationService.BusinessLogic
{
    public interface IConsultationPreperationService
    {
        ConsultationPreperation GetConsultationPreperation(Guid consultationPreperationGuid);

        ConsultationPreperation CreateConsultationPreperation(ConsultationPreperation consultationPreperation, Guid consultationGuid);

        ConsultationPreperation UpdateConsultationPreperation(ConsultationPreperation consultationPreperation);
        
        IEnumerable<ConsultationPreperation> GetAllConsultationPreperations(string patientssn);
        ConsultationPreperation GetNextConsultationPreperation(string patientssn);
    }
}