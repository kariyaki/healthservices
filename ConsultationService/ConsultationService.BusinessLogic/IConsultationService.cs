﻿using System;
using System.Collections.Generic;
using Contracts;

namespace ConsultationService.BusinessLogic
{
    public interface IConsultationService
    {
        Consultation CreateConsultation(Consultation consultation);

        IEnumerable<Consultation> GetAllConsultations();
        Consultation UpdateConsultation(Consultation consultation);
        Consultation GetConsultation(Guid consultationGuid);
        void DeleteConsultation(Guid consultationGuid);
    }
}