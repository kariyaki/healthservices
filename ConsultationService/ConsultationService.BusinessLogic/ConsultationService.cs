﻿using System;
using System.Collections.Generic;
using ConsultationService.Repository.Repository;
using Contracts;
using log4net;
using Newtonsoft.Json;

namespace ConsultationService.BusinessLogic
{
    public class ConsultationService : IConsultationService
    {
        private readonly ILog _logger;
        private readonly IConsultationRepository _consultationRepository;

        public ConsultationService(ILog logger, IConsultationRepository consultationRepository)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (consultationRepository == null) throw new ArgumentNullException(nameof(consultationRepository));
            _logger = logger;
            _consultationRepository = consultationRepository;
        }

        public Consultation CreateConsultation(Consultation consultation)
        {
            _logger.Debug("CreateConsultation");
            consultation.Guid = Guid.NewGuid();

            var consultationPreperation = new ConsultationPreperation
            {
                Guid = Guid.NewGuid(),
                ConsultationGuid = consultation.Guid,
                ConsultationTime = consultation.DateTime,
                Symptoms = new List<Symptom>(),
                OtherSubjectsList = new List<OtherSubject>()
            };

            consultation.ConsultationPreperation = consultationPreperation;

            consultation = _consultationRepository.Save(consultation);
            return consultation;
        }

        public IEnumerable<Consultation> GetAllConsultations()
        {
            _logger.Debug("GetAllConsultations");

            var consultations = _consultationRepository.FindAll();
            _logger.Debug(JsonConvert.SerializeObject(consultations));

            return consultations;
        }

        public Consultation UpdateConsultation(Consultation consultation)
        {
            _logger.Debug("UpdateConsultation");

            consultation.ConsultationPreperation.ConsultationTime = consultation.DateTime;

            consultation = _consultationRepository.Save(consultation);
            return consultation;
        }

        public Consultation GetConsultation(Guid consultationGuid)
        {
            _logger.Debug("GetConsultation");
            var consultation = _consultationRepository.Get(consultationGuid);
            return consultation;
        }

        public void DeleteConsultation(Guid consultationGuid)
        {
            _consultationRepository.Delete(_consultationRepository.Get(consultationGuid));
        }
    }
}