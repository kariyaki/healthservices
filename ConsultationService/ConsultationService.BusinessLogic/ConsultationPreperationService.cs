﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsultationService.Repository.Repository;
using Contracts;
using log4net;
using Newtonsoft.Json;

namespace ConsultationService.BusinessLogic
{
    public class ConsultationPreperationService : IConsultationPreperationService
    {
        private readonly ILog _logger;
        private readonly IConsultationRepository _consultationRepository;
        private readonly IConsultationPreperationRepository _consultationPreperationRepository;

        public ConsultationPreperationService(ILog logger, IConsultationRepository consultationRepository, 
            IConsultationPreperationRepository consultationPreperationRepository)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (consultationRepository == null) throw new ArgumentNullException(nameof(consultationRepository));
            if (consultationPreperationRepository == null)
                throw new ArgumentNullException(nameof(consultationPreperationRepository));
            _logger = logger;
            _consultationRepository = consultationRepository;
            _consultationPreperationRepository = consultationPreperationRepository;
        }

        public ConsultationPreperation GetConsultationPreperation(Guid consultationPreperationGuid)
        {
            _logger.Debug("GetConsultationPreperation");

            var consultationPrep = _consultationPreperationRepository.Get(consultationPreperationGuid);
            if (consultationPrep == null)
            {
                throw new ArgumentException();
            }
            _logger.Debug("ConsultationPrep: " + consultationPrep);

            return consultationPrep;
        }

        public ConsultationPreperation CreateConsultationPreperation(ConsultationPreperation consultationPreperation, Guid consultationGuid)
        {
            var appointment = _consultationRepository.Get(consultationGuid);
            consultationPreperation.ConsultationTime = appointment.DateTime;
            consultationPreperation.ConsultationGuid = appointment.Guid;
            if (consultationPreperation.Symptoms == null)
            {
                consultationPreperation.Symptoms = new List<Symptom>();
            }
            if (consultationPreperation.OtherSubjectsList == null)
            {
                consultationPreperation.OtherSubjectsList = new List<OtherSubject>();
            }
            consultationPreperation.Guid = Guid.NewGuid();
            appointment.ConsultationPreperation = consultationPreperation;

            var consultation = _consultationRepository.Save(appointment);
            return consultation.ConsultationPreperation;
        }

        public ConsultationPreperation UpdateConsultationPreperation(ConsultationPreperation consultationPreperation)
        {
            var updatedconsultationPreperation = _consultationPreperationRepository.Save(consultationPreperation);
            return updatedconsultationPreperation;
        }
        
        public IEnumerable<ConsultationPreperation> GetAllConsultationPreperations(string patientssn)
        {
            _logger.Debug("GetAllConsultationPreperations");

            var consultations = _consultationRepository.FindAll().Where(t => t.Patient.Ssn == patientssn).ToList();
            _logger.Debug(JsonConvert.SerializeObject(consultations));

            _logger.Debug("Consultation: " + consultations);

            var consultationPreperationList =  consultations.Select(consultation => consultation.ConsultationPreperation).ToList();
            _logger.Debug("ConsultationPreperationList: " + JsonConvert.SerializeObject(consultationPreperationList));

            return consultationPreperationList;;
        }

        public ConsultationPreperation GetNextConsultationPreperation(string patientssn)
        {

            var consultations = _consultationRepository.FindAll().Where(t => t.Patient.Ssn == patientssn).ToList();
            var sorted = consultations.OrderBy(t => t.DateTime).Where(t => t.DateTime.CompareTo(DateTime.Now) >= 0);
            _logger.Debug(JsonConvert.SerializeObject(sorted));

            var now = DateTime.Now;
            return (from consultation in sorted
                    where consultation.DateTime.CompareTo(now) == 0 
                    && consultation.DateTime.Hour > now.Hour 
                    && consultation.DateTime.Minute > now.Minute 
                    || consultation.DateTime.CompareTo(now) > 0
                    select consultation.ConsultationPreperation).FirstOrDefault();
        }
    }
}