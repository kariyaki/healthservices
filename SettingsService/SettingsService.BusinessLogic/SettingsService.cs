﻿using System;
using System.Linq;
using Contracts;
using log4net;
using SettingsService.Repository.Repository;

namespace SettingsService.BusinessLogic
{
    public class SettingsService : ISettingsService
    {
        private readonly ILog _logger;
        private readonly ISettingsRepository _settingsRepository;

        public SettingsService(ILog logger, ISettingsRepository settingsRepository)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (settingsRepository == null) throw new ArgumentNullException(nameof(settingsRepository));
            _logger = logger;
            _settingsRepository = settingsRepository;
        }

        public Contracts.Settings GetSettings()
        {
            try
            {
                var all = _settingsRepository.FindAll();
                return all.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error("Exception getting settings: " + ex);
                throw;
            }
        }

        public Settings UpdateSettings(Settings settings)
        {
            try
            {
                var update = _settingsRepository.Save(settings);
                return update;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception updating setting: " + ex);
                throw;
            }
        }
    }
}