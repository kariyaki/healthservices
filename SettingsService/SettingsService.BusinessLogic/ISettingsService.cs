﻿using Contracts;

namespace SettingsService.BusinessLogic
{
    public interface ISettingsService
    {
        Settings GetSettings();
        Settings UpdateSettings(Settings settings);
    }
}