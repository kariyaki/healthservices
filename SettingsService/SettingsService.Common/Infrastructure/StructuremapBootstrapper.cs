﻿using StructureMap;
using StructureMap.Graph;

namespace SettingsService.Common.Infrastructure
{
    public class StructuremapBootstrapper
    {
        public static IContainer Initialize()
        {
            return new Container(x =>
                x.Scan(scan =>
                {
                    scan.LookForRegistries();
                    scan.TheCallingAssembly();
                    scan.AssembliesFromApplicationBaseDirectory(y => y.FullName.Contains("Service"));
                    scan.WithDefaultConventions();
                }));
        }
    }
}