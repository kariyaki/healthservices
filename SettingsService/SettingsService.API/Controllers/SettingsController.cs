﻿using System;
using System.Web.Http;
using Contracts;
using log4net;
using SettingsService.BusinessLogic;

namespace SettingsService.API.Controllers
{
    [RoutePrefix("api/settings")]
    public class SettingsController : ApiController
    {
        private readonly ILog _logger;
        private readonly ISettingsService _settings;

        public SettingsController(ILog logger, ISettingsService settings)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (settings == null) throw new ArgumentNullException(nameof(settings));
            _logger = logger;
            _settings = settings;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetSettings()
        {
            _logger.Debug(nameof(GetSettings));
            var settings = _settings.GetSettings();
            return Ok(settings);
        }

        [HttpPut]
        [Route("")]
        public IHttpActionResult UpdateSettings(Settings settings)
        {
            _logger.Debug(nameof(UpdateSettings));
            var updated = _settings.UpdateSettings(settings);
            return Ok(updated);
        }

        [HttpGet]
        [Route("symptoms")]
        public IHttpActionResult GetSymptomsList()
        {
            _logger.Debug(nameof(GetSymptomsList));
            var symptoms = _settings.GetSettings().DefaultSymptoms;
            return Ok(symptoms);
        }

        [HttpGet]
        [Route("othersubjects")]
        public IHttpActionResult GetOtherSubjectsList()
        {
            _logger.Debug(nameof(GetOtherSubjectsList));
            var other = _settings.GetSettings().DefaultOtherSubjects;
            return Ok(other);
        }

        [HttpGet]
        [Route("preperationStart")]
        public IHttpActionResult GetPreperationStartWeeks()
        {
            _logger.Debug(nameof(GetPreperationStartWeeks));
            var preperationStartWeeks = _settings.GetSettings().ConsultationPreperationStart;
            return Ok(preperationStartWeeks);
        }
    }
}
