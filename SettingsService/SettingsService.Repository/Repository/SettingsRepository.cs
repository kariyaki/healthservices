﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts;
using log4net;
using Newtonsoft.Json;
using SettingsService.Repository.DAL;

namespace SettingsService.Repository.Repository
{
    public class SettingsRepository : ISettingsRepository
    {
        private readonly SettingsServiceContext _dbContext;
        private readonly ILog _logger;

        public SettingsRepository(SettingsServiceContext dbContext, ILog logger)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            _dbContext = dbContext;
            _logger = logger;
        }

        public Settings Get(long id)
        {
            var settings = _dbContext.Settings
                .Include(t => t.DefaultSymptoms)
                .Include(t => t.DefaultOtherSubjects)
                .FirstOrDefault(t => t.Id == id);
            return settings;
        }

        public Settings Save(Settings entity)
        {
            _logger.Debug("Saving Setting:" + JsonConvert.SerializeObject(entity));
            var old = Get(entity.Id);

            if (old != null)
            {
                HandleLists(entity, old);
                _dbContext.Entry(old).CurrentValues.SetValues(entity);
                old.DefaultSymptoms = new List<SettingsSymptom>(entity.DefaultSymptoms);
                old.DefaultOtherSubjects = new List<SettingsOtherSubject>(entity.DefaultOtherSubjects);
            }
            else
            {
                _dbContext.Entry(entity).State = EntityState.Added;
            }
            _logger.Debug("Saving...");
            _dbContext.SaveChanges();
            return entity;
        }

        private void HandleLists(Settings entity, Settings oldCopy)
        {
            _logger.Debug("old: " + JsonConvert.SerializeObject(oldCopy));
            _logger.Debug("new: " + JsonConvert.SerializeObject(entity));
            var remSymp = new List<SettingsSymptom>();
            var addSymp = new List<SettingsSymptom>();
            if (entity.DefaultSymptoms != null)
            {
                if (oldCopy.DefaultSymptoms != null)
                {
                    foreach (var oldSymptom in oldCopy.DefaultSymptoms.Except(entity.DefaultSymptoms))
                    {
                        remSymp.Add(oldSymptom);
                    }

                    foreach (var newSymptom in entity.DefaultSymptoms.Except(oldCopy.DefaultSymptoms))
                    {
                        addSymp.Add(newSymptom);
                    }
                }
                else
                {
                    foreach (var newSymptom in entity.DefaultSymptoms)
                    {
                        addSymp.Add(newSymptom);
                    }
                }
            }

            foreach (var symptom in remSymp)
            {
                _dbContext.SettingsSymptoms.Remove(symptom);
            }

            foreach (var symptom in addSymp)
            {
                _dbContext.SettingsSymptoms.Add(symptom);
            }

            var remOth = new List<SettingsOtherSubject>();
            var addOth = new List<SettingsOtherSubject>();
            if (entity.DefaultOtherSubjects != null)
            {
                if (oldCopy.DefaultOtherSubjects != null)
                {
                    foreach (var oldOther in oldCopy.DefaultOtherSubjects.Except(entity.DefaultOtherSubjects))
                    {
                        remOth.Add(oldOther);
                    }

                    foreach (var newOther in entity.DefaultOtherSubjects.Except(oldCopy.DefaultOtherSubjects))
                    {
                        addOth.Add(newOther);
                    }
                }
                else
                {
                    foreach (var newOther in entity.DefaultOtherSubjects)
                    {
                        addOth.Add(newOther);
                    }
                }
            }

            foreach (var oth in remOth)
            {
                _dbContext.SettingsOtherSubjects.Remove(oth);
            }

            foreach (var oth in addOth)
            {
                _dbContext.SettingsOtherSubjects.Add(oth);
            }
        }

        public IEnumerable<Settings> FindAll()
        {
            return _dbContext.Settings
                .Include(a => a.DefaultSymptoms)
                .Include(a => a.DefaultOtherSubjects)
                .ToList();
        }

        public void Delete(Settings entity)
        {
            _dbContext.Settings.Remove(entity);

            foreach (var symptom in entity.DefaultSymptoms)
            {
                _dbContext.SettingsSymptoms.Remove(symptom);
            }

            foreach (var other in entity.DefaultOtherSubjects)
            {
                _dbContext.SettingsOtherSubjects.Remove(other);
            }
            _dbContext.SaveChanges();
        }
    }
}