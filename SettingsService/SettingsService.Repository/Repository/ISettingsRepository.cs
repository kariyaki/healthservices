﻿using Contracts;

namespace SettingsService.Repository.Repository
{
    public interface ISettingsRepository : IRepository<Settings, long>
    {
    }
}