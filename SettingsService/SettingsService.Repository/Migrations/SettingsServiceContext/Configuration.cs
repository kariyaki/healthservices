using System.Collections.Generic;
using System.Data.Entity.Migrations;
using Contracts;

namespace SettingsService.Repository.Migrations.SettingsServiceContext
{
    internal sealed class Configuration : DbMigrationsConfiguration<DAL.SettingsServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            MigrationsDirectory = @"Migrations\SettingsServiceContext";
        }

        protected override void Seed(DAL.SettingsServiceContext context)
        {
            var settings = new Settings
            {
                ConsultationPreperationStart = 3,
                DefaultSymptoms = new List<SettingsSymptom>
                {
                     new SettingsSymptom
                        {
                            Id = 1,
                            Name = "Lammelse",
                            Change = "Er blitt v�rre i det siste",
                            Description = "Lammelse betyr.... kort men mer utfyllende forklaring",
                            Severity = "Mye ubehag/p�virker hverdagen i stor grad",
                            Important = true
                        }
                },
                DefaultOtherSubjects = new List<SettingsOtherSubject>
                {
                    new SettingsOtherSubject { Id = 1, Name= "Sp�rsm�l om MS-medisin"}
                }
            };
            context.Settings.AddOrUpdate(t => t.Id, settings);
        }
    }
}
