namespace SettingsService.Repository.Migrations.SettingsServiceContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ConsultationPreperationStart = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SettingsOtherSubject",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Settings_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Settings", t => t.Settings_Id)
                .Index(t => t.Settings_Id);
            
            CreateTable(
                "dbo.SettingsSymptom",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Severity = c.String(),
                        Change = c.String(),
                        Important = c.Boolean(nullable: false),
                        Settings_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Settings", t => t.Settings_Id)
                .Index(t => t.Settings_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SettingsSymptom", "Settings_Id", "dbo.Settings");
            DropForeignKey("dbo.SettingsOtherSubject", "Settings_Id", "dbo.Settings");
            DropIndex("dbo.SettingsSymptom", new[] { "Settings_Id" });
            DropIndex("dbo.SettingsOtherSubject", new[] { "Settings_Id" });
            DropTable("dbo.SettingsSymptom");
            DropTable("dbo.SettingsOtherSubject");
            DropTable("dbo.Settings");
        }
    }
}
