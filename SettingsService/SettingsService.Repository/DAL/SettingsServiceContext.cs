﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Contracts;
using Service.Repository.DAL;

namespace SettingsService.Repository.DAL
{
    public class SettingsServiceContext : BaseContext<SettingsServiceContext>
    {
        public DbSet<SettingsSymptom> SettingsSymptoms { get; set; }
        public DbSet<SettingsOtherSubject> SettingsOtherSubjects { get; set; }
        public DbSet<Settings> Settings { get; set; }
        
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}